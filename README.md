# Formation HDF5 pour la sismologie

Les équipes geodata et RESIF-SI organisent une formation autour du format de données [HDF5](https://fr.wikipedia.org/wiki/Hierarchical_Data_Format) appliqués à la sismologie.

## Programme

### 11 Juin 2019, 13h30

-   Prise en main du format HDF5 et de ses outils
    -   présentation du format
    -   découverte et prise en main des outils en ligne de commande ([h5tools](https://portal.hdfgroup.org/display/HDF5/Tools))
-   Mardi soir, restau à Grenoble (+ bière)

### 12 Juin 2019, 9h00

-   Manipuler HDF5 avec Python ([h5py](https://www.h5py.org/))
    -   formatage des données
    -   visualisation
    -   génération de HDF5 à partir de miniseed
-   Démonstration de la [suite PH5](https://github.com/PIC-IRIS/PH5/wiki) (générer du HDF5 à partir du format Segway)

## Prérequis

Nous disposons d'une salle de cours, l'accès au réseau se fera en wifi, par eduroam
Pour suivre la formation, il vous faut :

-   connaître les base UNIX (utiliser la ligne de commande, manipuler des répertoires et des fichiers)
-   disposer d'un ordinateur sous GNU/Linux
-   une installation de python 3.5 ou supérieur
-   un environnment virtual :
    
        python3 -m venv ~/.virtualenv/hdf5
-   modules python numpy, h5py, matplotlib
    
        source ~/.virtualenv/hdf5/bin/activate
        pip install --upgrade pip   # seulement si nécessaire
        pip install h5py matplotlib numpy
        pip install obspy
-   les outils h5tools et hdfview installés (paquet disponible dans vos distributions GNU/Linux)

## Lieu

Campus universitaire de Grenoble, Isterre, salle Thetys
<https://www.isterre.fr/french/l-institut/contact-et-acces/>

## Inscriptions

Pour vous inscrire, envoyez-moi un courriel <mailto:jonathan.Schaeffer@univ-grenoble-alpes.fr> avant le 31 mai 2019. Les places sont limitées (15 participants maximum).

## Équipe d'organisation

Intervenants : Albanne Lecointre, David Wolyniec

Organisation : Catherine Pequegnat, Jonathan Schaeffer

---

## Partage et publication des documents pour les formations CED
============================================================

Accès aux pages publiques
-------------------------

https://osug.gricad-pages.univ-grenoble-alpes.fr/RESIF/formations/hdf5

---

[![pipeline status](https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/formations/hdf5/badges/master/pipeline.svg)](https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/formations/hdf5/commits/master)

