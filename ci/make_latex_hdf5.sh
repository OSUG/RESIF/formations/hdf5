#!bin/bash

set -e

mkdir -p ./docs/pdf
## Formation hdf5
cd formation_hdf5/
pdflatex -shell-escape  --jobname=hdf5 main.tex
pdflatex -shell-escape  --jobname=hdf5 main.tex
#pdflatex -shell-escape  --jobname=published main.tex
#pdflatex -shell-escape  --jobname=published main.tex
#mv published.pdf hdf5_published.pdf
cp *.pdf ../docs/pdf/
