# Formation HDF5 pour la sismologie

---

## Pour compiler les sources latex localement

```
# en wifi
> docker run  -v /home/lecoinal/hdf5:/root/hdf5 --privileged --net="host" -ti tianon/latex@sha256:e8f4f35be81fda7ee838ac8c21e5cf32251f7e218a6157144b9d548bd6b5ac93 /bin/bash
# en filaire
> docker run  -v /home/lecoinal/hdf5:/root/hdf5 -ti tianon/latex@sha256:e8f4f35be81fda7ee838ac8c21e5cf32251f7e218a6157144b9d548bd6b5ac93 /bin/bash
> apt-get update -qq && apt-get install -y -qq python-pygments
> cd /root/hdf5/formation_hdf5
> pdflatex -shell-escape main.tex
```

---

## Partage des documents de cours

Accès au dernier pdf (cours) généré (latex) : 

[hdf5.pdf](https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/formations/hdf5/builds/artifacts/master/browse/docs/pdf?job=make_pdf_hdf5)



