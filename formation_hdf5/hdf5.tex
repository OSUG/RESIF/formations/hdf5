\begin{frame}
  \frametitle{HDF5 : Définition}

\textbf{Hierarchical Data Format}

Il s'agit d'un format de données ``intelligent`` pour stocker et organiser des données.

C'est à la fois:
\begin{itemize}
  \item un modèle de données
  \item une librairie et une API (interface de programmation applicative)
  \item un format de fichier
\end{itemize}
... pour la gestion des entrées-sorties.

\end{frame}

\begin{frame}
  \frametitle{HDF5 : Caractéristiques}
  \begin{figure}[htbp]
    \centering
    \includegraphics[height=0.85\textheight]{hdf5-benefits-wheel.png}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Caractéristiques}
\begin{itemize}
  \item Modèle de données auto-descriptif permettant la gestion de dataset complexe
  \item Format de données portable
  \item Format binaire, avec possibilité de compression additionnelle (avec ou sans perte) sur les datasets
  \item h5tools : outils en ligne de commande pour la gestion des fichiers et données HDF5
  \item Supporte les entrée-sorties parallèles (si associé à MPI-IO)
  \item API en C, C++, Fortran 90, Python (et Java, MatLab, ...)
\end{itemize}

\textweb{https://portal.hdfgroup.org/display/HDF5}

\textweb{http://docs.h5py.org/en/stable/}

\end{frame}

\subsection{Organisation des données et métadonnées}

\begin{frame}[fragile]
	\frametitle{Modèle de données auto-descriptif} % , portable et interopérable}
\begin{itemize}
  \item Possibilité d'organiser les datasets de façon hiérarchique, de créer des groupes / sous-groupes, de créer des liens (analogue à un système de fichier Linux/UNIX)
\end{itemize}
\begin{terminal}
  > h5ls -r fichier.h5 # Prints info, lists all objects recursively
  /                        Group
  /dset1                   Dataset {59, 2536}
  /B946.EHZ                External Link {/bettik/a/B946.h5/EHZ}
  /grp1                    Group
  /grp1/dset1              Dataset {59, 2536}
  /grp1/dset2              Dataset {59, 2536}
  /grp3                    Group
  /grp3/subgrp             Group
  /grp3/subgrp/dset1       Dataset {4, 5, 6}
  /grp3/subgrp/R7010.EPZ   External Link {./SG.h5/R7010.EPZ}
  [...]
\end{terminal}
\end{frame}

\begin{frame}[fragile]
	\frametitle{Modèle de données auto-descriptif} % , portable et interopérable}
\begin{itemize}
  \item Possibilité d'ajouter des métadonnées à un groupe ou à un dataset
\end{itemize}
\begin{terminal}
  > h5ls -rv ufinal.h5 # with verbosity (ici 2 métadonnées associées au root-group)
  /                        Group
      Attribute: domain\ lengths {2}
          Type:      native double
          Data:  1, 1
      Attribute: origins {2}
          Type:      native double
          Data:  0, 0
  /u                       Dataset {128/128, 128/128}
  [...]
\end{terminal}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Modèle de données auto-descriptif, portable et interopérable}
Le type de données et l'endianness de chaque dataset sont renseignés et stockés automatiquement à l'écriture des données, dans le header de chaque dataset. L'utilisateur n'a pas à remplir manuellement ces ``métadonnées``.
\begin{terminal}
  > h5ls -rv fichier.h5
  /                        Group
  /grp1                    Group
  /grp1/dset1              Dataset {59, 2536}
     Type:      native int
  /grp1/dset2              Dataset {59, 2536}
     Type:      native double
  # On peut mixer plusieurs données de types différents au sein d'un même fichier HDF5
  > h5dump -H -d /grp1/dset2 fichier.h5 # Prints Header for a given dataset 
  HDF5 "fichier.h5" {
  DATASET "/grp1/dset2" {
     DATATYPE  H5T_IEEE_F64LE
     DATASPACE  SIMPLE { ( 59, 2536 ) / ( 59, 2536 ) }
\end{terminal}
\end{frame}

% on peut mixer les types de données au sein d'un même fichier HDF5



\begin{frame}[fragile]
  \frametitle{Organisation des données}
\begin{itemize}
  \item Possibilité de créer des liens vers un dataset d'un autre fichier HDF5 (modif noms, groupes ...)
\end{itemize}

\begin{minted}{python}
h5f = h5py.File("out.h5","w")
h5f["/B946.EHZ"] = h5py.ExternalLink("/bettik/a/B946.h5", "EHZ")
h5fi = h5py.File("/bettik/b/SG.h5","r")
for name in h5fi:
  h5f["/"+name] = h5py.ExternalLink("/bettik/b/SG.h5", name)
h5fi.close()
h5f.close()
\end{minted}

\begin{terminal}
  > h5ls out.h5
  B946.EHZ     External Link {/bettik/a/B946.h5/EHZ}
  R0101.EPZ    External Link {/bettik/b/SG.h5/R0101.EPZ}
  [...]
  R7010.EPZ    External Link {/bettik/b/SG.h5/R7010.EPZ}
  > ls -lh out.h5 /bettik/b/SG.h5 /bettik/a/B946.h5
  -r--r--r-- 1 lecointre l-isterre 6.3M Aug 20 15:55 /bettik/a/B946.h5
  -r--r--r-- 1 lecointre l-isterre  28G May  7  2017 /bettik/b/SG.h5
  -rw-r--r-- 1 lecointre l-isterre 176K Aug 22 15:46 out.h5
\end{terminal}

\end{frame}

\subsection{Storage layout et Chunking}

\begin{frame}
  \frametitle{Storage layout et Chunking}

Il existe plusieurs types d'``agencement'' (\emph{storage layout}) d'un dataset sur le disque:
\begin{itemize}
  \item H5D\_COMPACT: écrit les données du dataset dans le header de l'objet dataset
  \begin{itemize}
    \item max dataset size = 64KB , pas de compression possible , pas d'IO //
  \end{itemize}
  \item H5D\_CONTIGUOUS: écrit les données du dataset séparément du header, en un seul morceau 
  \begin{itemize}
    \item problème pour les gros datasets extendible, vouloir rajouter un bout de données peut entrainer le déplacement de tout le dataset pour trouver une zone disque libre suffisament importante
    \item pas de compression possible
  \end{itemize}
  \item \emph{H5D\_CHUNKED}: écrit les données du dataset séparément du header, en plusieurs morceaux
  \begin{itemize}
    \item max nb of elements in a chunk = $2^{31}-1$ (max 4GB/chunk)
    \item compression possible
    \item IO // possible: essayer de mapper les chunks sur les vues des proc MPI qui accèderont à la donnée
  \end{itemize}
\end{itemize}
  \begin{figure}[htbp]
    \centering
    \includegraphics[height=0.3\textheight]{storage_layout.png}
  \end{figure}
\end{frame}


\begin{frame}[fragile]
  \frametitle{Storage layout et Chunking (exemple beam RESOLVE)}
\begin{terminal}
> h5ls -v resolve_beam.h5  # fichier avec un dataset de 80GB (h5d_contiguous impossible car >4GB)
beamformer           Dataset {53/53, 40/40, 5/5, 11/11, 172799/172799}
    Location:  1:10800
    Links:     1
    Chunks:    {4, 3, 1, 1, 10800} 518400 bytes # chunk size = 506.25KB
    Storage:   80593453600 logical bytes, 68016908569 allocated bytes, 118.49% utilization
    Filter-0:  deflate-1 OPT {4}
    Type:      native float
\end{terminal}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Storage layout et Chunking (exemple RESOLVE PH5)}
\begin{terminal}
> h5ls -av miniPH5_00001.ph5/Exp_g/Rec_g/Das_g_1X10/Data_a_2061
Data_a_2061              Dataset {1800000/Inf}
  Chunks:    {16384} 65536 bytes    # chunk size = 64KB
  Storage:   7200000 logical bytes, 2099949 allocated bytes, 342.87% utilization
  Filter-0:  shuffle-2 OPT {4}
  Filter-1:  deflate-1 OPT {6}
  Type:      native int
  Address: 4640269749
       Flags    Bytes     Address          Logical Offset
    ========== ======== ========== ==============================
    0x00000000    18816 4640271845 [0, 0]
    0x00000000    18870 4640290661 [16384, 0]  # 16384*4/18870=347%
    0x00000000    18859 4640309531 [32768, 0]
    0x00000000    18759 4640328390 [49152, 0]
    0x00000000    19053 4640347149 [65536, 0]
    0x00000000    18989 4640366202 [81920, 0]
    [...]
\end{terminal}
\end{frame}




\subsection{Compression}

\begin{frame}[fragile]
\frametitle{Compression sur les datasets}
Les données sont écrites en binaire dans les fichiers HDF5. On peut y ajouter différents filtres de compression (avec ou sans perte). Chaque dataset peut avoir son propre filtre.
\begin{terminal}
  > h5ls -rv out.h5 
  /01641                   Group
      Attribute: SVNRevision scalar
          Type:      17-byte null-terminated ASCII string
          Data:  "$Revision: 148 $"
  /01641/deplacement_horizontal Dataset {59/59, 2536/2536}
    Storage:   598496 logical bytes, 598496 allocated bytes, 100.00% utilization
    Type:      native int
  /01641/deplacement_vertical   Dataset {59/59, 2536/2536}
    Storage:   598496 logical bytes, 161746 allocated bytes, 370.02% utilization
    Filter-0:  deflate-1 OPT {6}
    Type:      native int
  /01641/energie_fenetre        Dataset {59/59, 2536/2536}
    Storage:   1196992 logical bytes, 630557 allocated bytes, 189.83% utilization
    Filter-0:  scaleoffset-6 OPT {0, 2, 129336, 1, 8, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    Type:      native double
\end{terminal}

\end{frame}

\begin{frame}[fragile]
\frametitle{Compression: Exemple données préprocessées 1-bit}
Cas des données japonaises préprocessées 1-bit: 
	\begin{itemize}
		\item groupes YEAR/JDAY/HOUR
		\item dataset horaire 2D: Fs=20Hz et 5x20min avec 10min overlap -> (5*24,000) samples/hour
		\item Int1 datatype + GZIP
	\end{itemize}
	\begin{terminal}
$ h5ls -r NRTH_SHN.h5
/                        Group
/2008                    Group
/2008/001                Group
	/2008/001/00             Dataset {5, 24000}
	/2008/001/01             Dataset {5, 24000}
	...
	/2012/366/22             Dataset {5, 24000}
	/2012/366/23             Dataset {5, 24000}
\end{terminal}
\end{frame}


\begin{frame}[fragile]
\begin{terminal}
$ h5ls -rv NRTH_SHN.h5
/                        Group
   Attribute: CLASS     scalar
      Type:      5-byte null-terminated ASCII string
      Data:  "GROUP"
   Attribute: PYTABLES_FORMAT_VERSION scalar
      Type:      3-byte null-terminated ASCII string
      Data:  "2.0"
   Attribute: TITLE     scalar
      Type:      1-byte null-terminated ASCII string
      Data:  ""
   Attribute: VERSION   scalar
      Type:      3-byte null-terminated ASCII string
      Data:  "1.0"
[...]
\end{terminal}
\end{frame}

\begin{frame}[fragile]
\begin{terminal}
/2008/001/00                Dataset {5/5, 24000/24000}
    Attribute: CLASS     scalar
       Type:      7-byte null-terminated ASCII string
       Data:  "CARRAY"
    Attribute: TITLE     scalar
       Type:      1-byte null-terminated ASCII string
       Data:  ""
    Attribute: VERSION   scalar
       Type:      4-byte null-terminated ASCII string
       Data:  "1.0"
    Location:  1:44618690
    Links:     1
    Chunks:    {2, 24000} 48000 bytes
    Storage:   120000 logical bytes, 18345 allocated bytes, 654.13% utilization
    Filter-0:  fletcher32-3  {}
    Filter-1:  shuffle-2 OPT {1}
    Filter-2:  deflate-1 OPT {1}
    Type:      native signed char
\end{terminal}
	Int1 : 5x24,000x1octet = Storage 120,000 logical bytes
\end{frame}

\begin{frame}[fragile]
  \frametitle{Compression: Exemple corrélations}
Possibilité de stocker/catégoriser/tagger avec métadonnées, des milliers de datasets dans un fichier

Ex: 19 days 10min-correlations (-2s:+2s) SanJacinto (1108nodes) : 1 fichier HDF5 d'1.6TB contenant 600,000 correlations:
	\begin{itemize}
		\item organisées en groupes: paire de stations, paire de channels
		\item 2D dataset (time, corrtime)
		\item encodées en F32, compressées en GZIP
	\end{itemize}
	\begin{terminal}
$ ls -lh /summer/f_image/lecointre/out.h5 
-rw-r--r-- 1 113484 pr-f-image 1.6T Mar  9 14:21 out.h5
$ h5ls -v out.h5/R0101_R5002/EPZ_EPZ/corr
Opened "out.h5" with sec2 driver.
corr                     Dataset {2736/2736, 401/401}
    Location:  1:2958867846
    Links:     1
    Chunks:    {171, 51} 34884 bytes
    Storage:   4388544 logical bytes, 3996364 allocated bytes, 109.81% utilization
    Filter-0:  deflate-1 OPT {4}
    Type:      native float
	\end{terminal}
\end{frame}

\begin{frame}[fragile]
	\begin{terminal}
$ h5ls -av out.h5/R0101_R5002/EPZ_EPZ/corr
corr                     Dataset {2736/2736, 401/401}
    Location:  1:2958867846
    Chunks:    {171, 51} 34884 bytes
    Storage:   4388544 logical bytes, 3996364 allocated bytes, 109.81% utilization
    Filter-0:  deflate-1 OPT {4}
    Type:      native float
    Address: 2959762083
           Flags    Bytes     Address          Logical Offset
        ========== ======== ========== ==============================
        0x00000000    31635 2959764699 [0, 0, 0]
        0x00000000    31719 2959796334 [0, 51, 0]
        0x00000000    31592 2959828053 [0, 102, 0]
        0x00000000    31646 2959859645 [0, 153, 0]
\end{terminal}
	Note: en pratique, on essaie de ne jamais écrire de corrélations sur disque (on préfère les (re)-calculer à la volée). si c'est vraiment nécessaire, on préfèrera utiliser un algo de compression avec perte de type scale-offset\footnote{\tiny Pour des pistes de réflexion sur l'encodage et/ou la compression des corrélations sur disque: voir par exemple \textweb{https://ciment.ujf-grenoble.fr/wiki/index.php/Projects/pr-sanjacinto\#Compression\_of\_correlations\_output\_.3F}}.
\end{frame}





\begin{frame}[fragile]
\frametitle{Compression: Suite exemple écriture des corrélations}
Init : Daily xcorr 17025pairs × 4 (F32) × 144 (10min freq) × 401 (2sec lag at 100Hz) : 3.8GB

Method 0 : enable GZ compression
\begin{itemize}
\item lossless compression
\item 3.2GB → 17\% compression rate (en moyenne sur un fichier de 17205 datasets)
\item large "cpu" overhead (x1.79) and no significant memory overhead (x1.1)
\end{itemize}

Method 1 : activate the scaleoffset compression (HDF5 features) 
Scale-offset filter - performs a scale and offset on each data value truncating the resulting value to a fixed number of bits before storing. 
$Dnew = 10^{scale} * Dold + offset$
\begin{itemize}
\item lossy (as we have floating point data)
\item nn\_scaleoffset=5 allows 1.7GB filesize → 57\% compression rate
\item smaller "cpu" overhead (x1.14) and no significant memory overhead (x1.1)
\end{itemize}
\end{frame}

\begin{frame}
  \begin{figure}[htbp]
    \centering
    \includegraphics[height=0.8\textheight]{overhead_compression_gzip_scaleoffset.png}
  \end{figure}
	overhead "cpu" lié à la compression + gain "elapsed" (on en écrit moins). Ici un seul job séquentiel sur un scratch "perso". En pratique : compresser efficacement sera toujours bénéfique pour du traitement massif de données effectué en parallèle et sur FileSystem partagé avec beaucoup d'autres utilisateurs.
\end{frame}









%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../calcul_parallele"
%%% End:
