#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

# python3.5 readmseed_writehdf5.py

import h5py
import sys
from obspy.core import read
import requests
import io


# cette serie temporelle: l'exemple de la doc timeseries webservice RESIF
# http://ws.resif.fr/resifws/timeseries/1/query?net=FR&cha=HHZ&start=2018-02-12T03:08:02&end=2018-02-12T03:10:00&sta=CIEL&loc=00&out=plot
example_url = "http://ws.resif.fr/fdsnws/dataselect/1/query?net=FR&cha=HHZ&start=2018-02-12T03:08:02&end=2018-02-12T03:10:00&sta=CIEL&loc=00"

# cette serie temporelle : une heure avec trous, ca marche aussi
# http://ws.resif.fr/resifws/timeseries/1/query?net=FR&cha=HHZ&start=2018-09-20T08:00:00&end=2018-09-20T09:00:00&sta=CIEL&loc=00&out=plot
#example_url = "http://ws.resif.fr/fdsnws/dataselect/1/query?net=FR&cha=HHZ&start=2018-09-20T08:00:00&end=2018-09-20T09:00:00&sta=CIEL&loc=00"

stringio_obj = io.BytesIO(requests.get(example_url).content)

st = read(stringio_obj)

print(st)  

# Get metadata
net = st[0].stats.network
sta = st[0].stats.station
loc = st[0].stats.location
channel = st[0].stats.channel
year = st[0].stats.starttime.year
jd = st[0].stats.starttime.julday
datatype = st[0].data.dtype

# Fill gaps if any
st.merge(method=0, fill_value=0)

print(st)

# Write data (only first channel in this example) as HDF5 dataset
SDSdir = '.'
SDSdset = net+'.'+sta+'.'+loc+'.'+channel+'.D.'+"{:04d}".format(year)+'.'+"{:03d}".format(jd)
h5f = h5py.File(SDSdir+'/'+net+'_'+"{:04d}".format(year)+'_'+"{:03d}".format(jd)+'.h5','w')
#h5f.create_dataset(SDSdset,shape=(st[0].stats.npts,), dtype=datatype,data=st[0].data[:],compression='gzip', compression_opts=6, fletcher32='True', shuffle='True')
h5f.create_dataset(SDSdset,shape=(st[0].stats.npts,), dtype=datatype,data=st[0].data[:],compression='gzip', compression_opts=6, fletcher32='True')
h5f.close()

# ensuite on regarde la trace avec HDFview et on compare a resifws timeseries 
