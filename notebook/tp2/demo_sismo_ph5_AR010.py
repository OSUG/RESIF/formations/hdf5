#!/usr/bin/env python
# coding: utf-8

# # Un exemple d'utilisation de fichier sismo ph5

# On veut comparer : 
# * les données RESOLVE PH5
# * les données RESOLVE MSEED qui ont été converties (with gap-filling -> 0) en HDF5 : EXP000 : https://gricad-gitlab.univ-grenoble-alpes.fr/lecoinal/resolve/wikis/preprocess
# 
# # Les données RESOLVE PH5
# 
# On a extrait les heures suivantes :
#
# 1. Le début de la serie temporelle: 
# Data_a_1661 : 114,13:10:44.538 : startpoint 0-based index = (13*3600+10*60+44.538)*500 = 23,722,269
# Data_a_1662 : 114,14:10:44.538
# Data_a_1663 : 114,15:10:44.538
# 
# 2. deux heures qui se suivent dont une à cheval sur deux jours
# Data_a_2079 : 131,23:10:44.538
# Data_a_2080 : 132,00:10:44.538
#
# 3. La dernière heure
# Data_a_2490 : 149,02:10:44.538
# 
# > h5copy -i /summer/resolve/Argentiere/DATA/PH5/miniPH5_00001.ph5 -o /tmp/miniPH5_AR010_Z.ph5 -p -s /Experiment_g/Receivers_g/Das_g_1X10/Das_t -d /Das_t
# 
# >  for jj in "1661 1662 1663 2079 2080 2490" ; do h5copy -i /summer/resolve/Argentiere/DATA/PH5/miniPH5_00001.ph5 -o /tmp/miniPH5_AR010_Z.ph5 -s /Experiment_g/Receivers_g/Das_g_1X10/Data_a_$jj -d /Data_a_$jj ; done

import h5py
import numpy as np
import matplotlib.pyplot as plt

h5f = h5py.File('miniPH5_AR010_Z.ph5', 'r')
print(h5f.keys())

myh5f = h5py.File('ZO_2018_AR010_gzip6_114_131_132_149.h5','r')
print(myh5f.keys())

def read_one_hour_ph5(h5file,ti):
    data_1h = h5f['Data_a_'+ti][:]
    return data_1h

def read_one_hour_h5(h5file,jd,hh):
    offsetindex = int(500*(hh*3600+10*60+44.538))
    data_1h = h5file['ZO.AR010.00.DPZ.D.2018.'+jd][offsetindex:offsetindex+500*3600]
    return data_1h

def make_timeplot(d1, d2, titre):
    plt.clf()
    plt.subplot(311)
    plt.plot(d1)
    plt.title(titre)
    plt.ylabel('PH5')
    plt.subplot(312)
    plt.plot(d2)
    plt.ylabel('H5')
    plt.subplot(313)
    plt.plot(d1[:]/d2[:])
    plt.ylabel('PH5/H5')
    #plt.savefig(titre+'.png')
    plt.show()

def make_scatterplot(d1, d2, titre):
    plt.clf()
    plt.plot(d1,d2,'.')
    plt.xlabel('PH5')
    plt.ylabel('H5')
    plt.title('s_'+titre)
    #plt.savefig('s_'+titre+'.png')
    plt.show()

def make_all(ph5f, ti, h5f, jd, hh):
    ph5data = read_one_hour_ph5(ph5f, ti)
    h5data = read_one_hour_h5(h5f, jd, hh)
    print(np.nanmin(ph5data[:]/h5data[:]))
    print(np.nanmax(ph5data[:]/h5data[:]))
    titre = ti+'_'+jd+'_'+'{:02d}'.format(hh)+':10:44_538'
    make_timeplot(ph5data,h5data,titre)
    make_scatterplot(ph5data,h5data,titre)

# Les 3 premières heures

timeindex = '1661'
jday = '114'
hour = 13
make_all(h5f, timeindex, myh5f, jday, hour)

timeindex = '1662'
jday = '114'
hour = 14
make_all(h5f, timeindex, myh5f, jday, hour)

timeindex = '1663'
jday = '114'
hour = 15
make_all(h5f, timeindex, myh5f, jday, hour)

# La derniere heure

timeindex = '2490'
jday = '149'
hour = 2
make_all(h5f, timeindex, myh5f, jday, hour)

# 2h à cheval sur deux jours

time = np.arange(131+(23*3600+10*60+44.538)/86400,132+(1*3600+10*60+44.536)/86400,0.002/86400)

timeindex = '2079'
ph5data = h5f['Data_a_'+timeindex][:]
timeindex = '2080'
ph5data = np.concatenate((ph5data,h5f['Data_a_'+timeindex][:]))

jday = '131'
offsetindex = int(500*(23*3600+10*60+44.538))
h5data = myh5f['ZO.AR010.00.DPZ.D.2018.'+jday][offsetindex:43200000]
jday = '132'
offsetindex = int(500*(1*3600+10*60+44.538))
h5data = np.concatenate(( h5data , myh5f['ZO.AR010.00.DPZ.D.2018.'+jday][0:offsetindex]))

print(np.nanmin(ph5data[:]/h5data[:]),np.nanmax(ph5data[:]/h5data[:]))

plt.clf()
plt.plot(ph5data,h5data,'.')
plt.xlabel('PH5')
plt.ylabel('H5')
#plt.savefig('2079_2080_131_23:10:44_538_132_01:10:44_536.png')
plt.show()

plt.clf()
plt.subplot(211)
plt.plot(time,ph5data)
plt.title('PH5')
plt.subplot(212)
plt.plot(time,h5data)
plt.title('H5')
#plt.savefig('2079_2080_131_23:10:44_538_132_01:10:44_536.png')
plt.show()



# jonction 131 132
ib = 1477500 # 0
ie = 1478000 # 3600000
plt.clf()
plt.subplot(311)
plt.plot((time[ib:ie]-132)*86400,ph5data[ib:ie])
plt.subplot(312)
plt.plot((time[ib:ie]-132)*86400,h5data[ib:ie])
plt.subplot(313)
plt.plot((time[ib:ie]-132)*86400,ph5data[ib:ie]/h5data[ib:ie])
#plt.savefig('2079_jonction_131_132.png')
plt.show()

# jonction entre 2 ph5 dataset
ib = 1800000-10 # 0
ie = 1800000+10 # 3600000
plt.clf()
plt.subplot(311)
plt.plot((time[ib:ie]-132)*86400,ph5data[ib:ie],'.-')
plt.subplot(312)
plt.plot((time[ib:ie]-132)*86400,h5data[ib:ie],'.-')
plt.subplot(313)
plt.plot((time[ib:ie]-132)*86400,ph5data[ib:ie]/h5data[ib:ie],'.-')
#plt.savefig('2079_2080_jonction.png')
plt.show()
