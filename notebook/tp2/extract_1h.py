import h5py
import numpy as np

offset = 9322269 # 0-based index

h5fi = h5py.File('ZO_2018_131.h5','r')

h5fo = h5py.File('ZO_2018_131_1h.h5','w')

print(h5fi.keys())

data1h = h5fi['/ZO.AR010.00.DPZ.D.2018.131'][offset:offset+500*3600]

print(data1h)
print(np.shape(data1h))

h5fo.create_dataset('/ZO.AR010.00.DPZ.D.2018.131', data=data1h, compression="gzip")

h5fi.close()
h5fo.close()
